﻿
using UnityEngine;

public class LevelGenrator : MonoBehaviour
{
    public Texture2D map;
    public ColortoPrefeb[] colorMapping;
    public GameObject goc;
    // Start is called before the first frame update
    void Start()
    {
        GenerateLevel();
    }
    private void GenerateLevel()
    {
        for(int x = 0; x < map.width; x++)
        {
            for (int y = 0; y < map.height; y++)
            {
                GenerateTile(x, y);
            }
        }
    }
    private void GenerateTile(int x,int y)
    {
        Color pixelColor = map.GetPixel(x, y);
        if (pixelColor.a == 0)
        {
            return;
        }
        foreach(ColortoPrefeb colormapping in colorMapping)
        {
            if (colormapping.color.Equals(pixelColor))
            {
                Vector2 pos = new Vector2(goc.transform.position.x + x, goc.transform.position.y + y);
                Instantiate(colormapping.prefab, pos, Quaternion.identity, transform);
            }
        }
    }
}
