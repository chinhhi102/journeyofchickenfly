﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScrips : MonoBehaviour
{
    private LevelManager gamelevelManager;
    private CameraController cmr;
    public int valueCoins;
    public int loai;
    private void Start()
    {
        gamelevelManager = FindObjectOfType<LevelManager>();
        cmr = FindObjectOfType<CameraController>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player1")
        {
            switch (loai)
            {
                case 0:
                    {
                        break;
                    }
                case 1:
                    {
                        gamelevelManager.addCoins(valueCoins);
                        break;
                    }
                case 2:
                    {
                        cmr.GenerateImg(1, 0);
                        break;
                    }
            }
            Destroy(gameObject);
        }
    }
}
