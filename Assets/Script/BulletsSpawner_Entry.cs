﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsSpawner_Entry : MonoBehaviour
{
    [SerializeField]
    private GameObject _Bullets;
    // Start is called before the first frame update
    public void Start()
    {
        StartCoroutine(Spawner());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator Spawner()
    {
        yield return new WaitForSeconds(1);
        Vector3 temp = _Bullets.transform.position;
        temp.x += 30f;
        temp.y = Random.Range(3.5f, 5.5f);
        Instantiate(_Bullets, temp, Quaternion.identity);
        temp.y = Random.Range(-3.5f, 3.5f);
        Instantiate(_Bullets, temp, Quaternion.identity);
        temp.y = Random.Range(-5.5f, -4.5f);
        Instantiate(_Bullets, temp, Quaternion.identity);
        StartCoroutine(Spawner());
    }
}
