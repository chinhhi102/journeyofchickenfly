﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMove : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed=5;

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(-Vector2.right * speed * Time.deltaTime);
    }
}
